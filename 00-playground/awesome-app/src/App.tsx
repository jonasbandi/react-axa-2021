import React from 'react';
import './App.css';
import { Greeter } from './Greeter';

function App() {

    const a = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const items = a
        .filter(e => e % 2 === 0)
        .map((a, index) => (
            <li key={index}>
                {a}
                {/*<Greeter/>*/}
            </li>));

    let content: JSX.Element
    let showGreeter = true;
    if (new Date().getMilliseconds() % 2 === 0) {
        content = <h1>Hello AXA</h1>;
        // showGreeter = true;

    } else {
        content = <h1>Goodbye!</h1>
        // showGreeter = false;
    }

    console.log('Rendring App');
    return (
        <>
            <div className="App">
                {content}
                { showGreeter ?  <Greeter/> : null }
                <ul>
                    {items}
                </ul>
            </div>
        </>
    );
}

export default App;
