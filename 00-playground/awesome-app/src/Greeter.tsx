import React, {useState} from 'react';
import { myAdder, myLogOut, Person, TEST, theSingleKing } from './utils';

export function Greeter() {

    const [name, setName] = useState('');
    const [date, setDate] = useState(new Date());
    const [count, setCount] = useState(1);

    function handleClick() {

        myLogOut('Button Clicked!' + new Date());
        setDate(new Date());
        setCount((prevCount) => prevCount + 1);
        setCount((prevCount) => prevCount + 1);
        console.log('I triggered a rerender', count);
    }

    function handleKeyUp(event: any) {
        console.log('Key Up', event.target.value);
        setName(event.target.value);
    }

    console.log('Rendering Greeter');
    return (
        <div style={{border: "solid 3px red", padding: "10px"}}>
            <h1>Hello {name}</h1>
            <h1>Count: {count}</h1>
            <h2>{date.toISOString()}</h2>
            <button onClick={() => handleClick()}>Click me!</button>
            <div>

                <input onKeyUp={handleKeyUp}/>
            </div>
        </div>
    );
}
