import { ChangeEvent, FormEvent, useState } from 'react';

interface NewToDoProps {
    onAddToDo: (newToDoTitle: string) => void
}

export function NewToDo(props: NewToDoProps) {

    const [toDoTitle, setToDoTitle] = useState<string>('');

    const inputChange = (e: ChangeEvent<HTMLInputElement>) => {
      setToDoTitle(e.target.value);
    };

    let submitButton = null;
    if (toDoTitle.length > 3) {
      submitButton = <button id="add-button" className="add-button">+</button>;
    }

    function addToDo(e: FormEvent) {
        e.preventDefault();
        props.onAddToDo(toDoTitle);
        setToDoTitle('');
    }

    return (
        <form className="new-todo" onSubmit={addToDo}>
            <input id="todo-text" name="toDoTitle" type="text" placeholder="What needs to be done?"
                   autoFocus
                   autoComplete="off"
                   value={toDoTitle}
                   onChange={inputChange}
            />
            {submitButton}
        </form>
    );
}
