import { lazy, Suspense } from 'react';
import { BrowserRouter as Router, NavLink } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
// import  ToDoScreen  from './components/ToDoScreen';
// import  DoneScreen } from './components/DoneScreen';

const ToDoScreen = lazy(() => import('./components/ToDoScreen'));
const DoneScreen = lazy(() => import('./components/DoneScreen'));

export function App() {
  const queryClient = new QueryClient();

  return (
    <div className="App">
      <div className="todoapp-header">
        <h1 id="title">Simplistic ToDo</h1>
        <h4>A most simplistic ToDo List in React.</h4>
      </div>

      <section className="todoapp">
        <QueryClientProvider client={queryClient}>
          <Router>
            <div className="nav">
              <NavLink exact to="/" activeClassName="selected">
                Pending
              </NavLink>
              <NavLink exact to="/done" activeClassName="selected">
                Done
              </NavLink>
            </div>

            <Suspense fallback={<div>Loading ...</div>}>
              <Switch>
                <Route path="/done">
                  <DoneScreen />
                </Route>
                <Route path="/">
                  <ToDoScreen />
                </Route>
              </Switch>
            </Suspense>
          </Router>
          <ReactQueryDevtools initialIsOpen={true} />
        </QueryClientProvider>
      </section>

      <footer className="info">
        <p>
          JavaScript Example / Initial template from <a href="https://github.com/tastejs/todomvc-app-template">todomvc-app-template</a>
        </p>
      </footer>
    </div>
  );
}
