import axios from 'axios';
import { useEffect, useState } from 'react';
import { ToDo, ToDoPostResponse, ToDosGetResponse } from '../api/types';
import { useQuery, useMutation, useQueryClient } from 'react-query';

const API_URL = 'http://localhost:3456/todos';

async function load(completed: boolean) {
  const serverResponse = await axios.get<ToDosGetResponse>(API_URL, { params: { completed: completed ? 1 : 0 } });
  return serverResponse.data.result;
}

const TODO_QUERY_KEY = 'todos';

export function useToDos(completed = false) {
  const { isLoading, error, data } = useQuery<ToDo[]>([TODO_QUERY_KEY, completed], () => load(completed));

  // console.log('useToDos');
  // const [loading, setLoading] = useState(true);
  // const [todos, setTodos] = useState<ToDo[]>([]);
  //
  // useEffect(() => {
  //   // using a nested async function since the effect function is not allowed to return a value (except the cleanup function)
  //   // and async functions implicitly return a promise
  //   async function load() {
  //     console.log('Loading ToDos from API');
  //     const todos = await loadToDos();
  //     setTodos(todos);
  //     setLoading(false);
  //   }
  //   load();
  // }, []);
  console.log('use todos', data);
  return { isLoading, todos: data ?? [] };
}

export function useAddToDoMutation() {
  const queryClient = useQueryClient();
  return useMutation((toDo: ToDo) => axios.post<ToDoPostResponse>(API_URL, toDo), {
    onSuccess() {
      queryClient.invalidateQueries(TODO_QUERY_KEY);
    }
  });
}

export function useCompleteToDoMutation() {
  const queryClient = useQueryClient();
  return useMutation(
    (toDo: ToDo) => {
      toDo.completed = true;
      return axios.put(`${API_URL}/${toDo.id}`, toDo);
    },
    {
      onSuccess() {
        queryClient.invalidateQueries(TODO_QUERY_KEY);
      }
    }
  );
}

export function useDeleteToDoMutation() {
  const queryClient = useQueryClient();
  return useMutation(
    (toDo: ToDo) => {
      return axios.delete(`${API_URL}/${toDo.id}`);
    },
    {
      onSuccess() {
        queryClient.invalidateQueries(TODO_QUERY_KEY);
      }
    }
  );
}

export async function loadToDos(completed = 0) {
  const serverResponse = await axios.get<ToDosGetResponse>(API_URL, { params: { completed } });
  return serverResponse.data.result;
}

export async function saveToDo(toDo: ToDo) {
  try {
    const serverResponse = await axios.post<ToDoPostResponse>(API_URL, toDo);
    return serverResponse.data.result;
  } catch {
    alert('Something went terribly wrong!');
    window.location.reload();
  }
}

export async function updateToDo(toDo: ToDo) {
  try {
    await axios.put(`${API_URL}/${toDo.id}`, toDo);
  } catch (error) {
    console.log(error);
  }
}

export async function deleteToDo(toDo: ToDo) {
  try {
    await axios.delete(`${API_URL}/${toDo.id}`);
  } catch (error) {
    console.log(error);
  }
}
