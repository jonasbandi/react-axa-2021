import React, { useEffect, useState } from 'react';
import { ToDoList } from './ToDoList';
import { ToDo } from '../api/types';
import { loadToDos, deleteToDo, useToDos, useDeleteToDoMutation } from '../api/persistence';

function DoneScreen() {
  const [message, setMessage] = useState('');
  const deleteToDoMutation = useDeleteToDoMutation();

  async function removeToDo(toDo: ToDo) {
    setMessage('Saving ...');
    deleteToDoMutation.mutate(toDo);

    setMessage('');
  }

  const loadingIndicator = message ? <div>{message}</div> : null;

  return (
    <div>
      {deleteToDoMutation.isLoading && <div>Saving ...</div>}
      <div className="main">
        <ToDoList completed={true} onRemoveToDo={removeToDo} />
      </div>
    </div>
  );
}

export default DoneScreen;
