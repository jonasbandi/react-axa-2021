import ToDoListItem from './ToDoListItem';
import { ToDo } from '../api/types';
import { useToDos } from '../api/persistence';

type ToDoListProps = {
  completed: boolean;
  onRemoveToDo: (toDo: ToDo) => void;
};

export function ToDoList({ completed, onRemoveToDo }: ToDoListProps) {
  const { todos, isLoading } = useToDos(completed);

  return (
    <>
      {isLoading && <div>Loading ...</div>}
      <ul id="todo-list" className="todo-list">
        {todos.map(t => (
          <ToDoListItem key={t.id} todo={t} onRemoveToDo={onRemoveToDo} />
        ))}
      </ul>
    </>
  );
}
