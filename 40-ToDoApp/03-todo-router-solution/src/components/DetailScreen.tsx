import React from 'react';
import { Link, useParams } from 'react-router-dom';

function DetailScreen() {
  const { id } = useParams<any>();

  return (
    <div>
      <h3>Detail for {id}</h3>
      <div>
        <Link to="/">back</Link>
      </div>
      <form className="new-todo">Not yet implemented ...</form>
    </div>
  );
}

export default DetailScreen;
